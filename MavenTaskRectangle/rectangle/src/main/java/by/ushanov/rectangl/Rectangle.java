package by.ushanov.rectangl;

public class Rectangle {
    public int side1,side2;

    public Rectangle(){}

    //Формула для нахождения площади
    public int areaCalculator(int num1,int num2){
        side1 = (num1 * num2);
        return side1;
    }

    //Формула для нахождения периметра прямоугольника
    public int perimeterCalculator(int num1,int num2){
        side2 = ((num1 + num2)*2);
        return side2;
    }

    public void viewResult(){
        System.out.println("Площадь прямоугольника: "+side1);
        System.out.println("Периметра прямоугольника: "+side2);
    }
}

