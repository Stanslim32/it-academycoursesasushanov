package by.ushanov;

import by.ushanov.rectangl.Rectangle;

import java.util.Scanner;


public class App
{
    public static void main(String[] args) {
        Rectangle rec = new Rectangle();
        Scanner in = new Scanner(System.in);

        System.out.println("Введите длину прямоугольника: ");
        int num1 = in.nextInt();
        System.out.println("Введите ширину прямоугольника: ");
        int num2 = in.nextInt();
        if ((num1 | num2) <= 0) {
            System.out.println("Значение не может быть отрицательным, либо быть равно нулю.");
        } else {
            rec.areaCalculator(num1, num2);
            rec.perimeterCalculator(num1, num2);
            rec.viewResult();
        }
    }
}
